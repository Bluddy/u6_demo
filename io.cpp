#include <iostream>
#include <vector>
#include <map>
//#include <pair>

using namespace std;

int main() {
    vector<int> v {10, 12, 20, 50};
    v[1] = 14;
    for (int i; i<v.size(); i++) {
        cout << v[i] << ' ';
    }

    cout << '\n';

//    pair<int, float> p {10, 20.5};
//   cout << p.first << ' ' << p.second << endl;

    vector<int>::iterator it;
    for (it = v.begin(); it < v.end(); it++) {
        cout << *it;
    }

    cout << '\n';

    map<int, string> m {{1, "one"}, {2, "two"}, {10, "ten"}};
    m[2] = "not two";
    cout << "map of 2 is " << m[2] << endl;
    map<int, string>::iterator mit = m.find(2);
    if (mit == m.end()) {
        cout << "not found\n";
    } else {
        cout << mit->first << "=>" << (mit->second) << endl;
    }




}
